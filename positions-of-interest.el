;;; positions-of-interest.el --- 

;; Copyright (C) 2020, Paul Horton, All rights reserved.
;; License: GNU GPLv3
;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20200129
;; Updated: 20200129
;; Version: 0.0
;; Keywords: 

;;; Commentary:

;; When I created this file I was not aware that of the aliases
;; point-at-bol and point-at-el in subr.el which are good enough names I think.

;; So although I like the brevity of bolpos and eolpos, I possibly will not
;; use them anymore.

;;; Change Log:

;;; Code:

(defalias 'bolpos 'line-beginning-position); AKA point-at-bol
(defalias 'eolpos 'line-end-position); AKA point-at-eol

(provide 'positions-of-interest)


;;; positions-of-interest.el ends here
